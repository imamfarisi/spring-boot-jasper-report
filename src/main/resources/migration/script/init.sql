drop table IF EXISTS student;

create table student(
	id serial primary key,
	full_name varchar,
	test_time date
);

INSERT INTO student (full_name, test_time) VALUES 
	('Mahasiswa 1', now()),
	('Mahasiswa 2', now()),
	('Mahasiswa 3', now()),
	('Mahasiswa 4', now()),
	('Mahasiswa 5', now()),
	('Mahasiswa 6', now()),
	('Mahasiswa 7', now()),
	('Mahasiswa 8', now()),
	('Mahasiswa 9', now());