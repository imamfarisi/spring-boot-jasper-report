package com.test.jasper.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.jasper.dao.StudentDao;
import com.test.jasper.model.Student;

@Service
@Transactional
public class StudentService {

	@Autowired
	private StudentDao mahasiswaDao;

	public List<Student> getAllMhs() throws Exception {
		return mahasiswaDao.getAllMhs();
	}
}
