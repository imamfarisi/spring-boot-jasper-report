package com.test.jasper.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.test.jasper.model.Student;
import com.test.jasper.repo.StudentRepo;

@Repository
public class StudentDao {

	@Autowired
	private StudentRepo mhsRepo;

	public List<Student> getAllMhs() throws Exception {
		return mhsRepo.findAll();
	}
}
