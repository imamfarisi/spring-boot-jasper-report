package com.test.jasper.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import liquibase.integration.spring.SpringLiquibase;

@Configuration
public class ObjectConfig {

	@Bean
	public SpringLiquibase liquibase(@Autowired DataSource dataSource) {
	    SpringLiquibase liquibase = new SpringLiquibase();
	    liquibase.setChangeLog("classpath:migration/script/init.sql");
	    liquibase.setDataSource(dataSource);
	    return liquibase;
	}
	
}
