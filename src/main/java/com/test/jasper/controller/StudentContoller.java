package com.test.jasper.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.jasper.model.Student;
import com.test.jasper.service.StudentService;
import com.test.jasper.util.JasperUtil;

@RestController
@RequestMapping("students")
public class StudentContoller {

	@Autowired
	private StudentService studentService;

	@GetMapping("report")
	public ResponseEntity<?> reportSample() throws Exception {
		List<Student> listData = studentService.getAllMhs();
		
		Map<String, Object> map = new HashMap<>();
		map.put("company", "PT. Company Internasional");

		byte[] out = JasperUtil.responseToByteArray(listData, map, "sample");
		
		String fileName = "report.pdf";
		
		return ResponseEntity.ok()
				.contentType(MediaType.APPLICATION_PDF)
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName+ "\"")
				.body(out);
	}

}
