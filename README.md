## How to Start
1. Create database with name "dummy" in your PostgreSQL (you can change the setting in application.properties if you like)
2. Run the Class App as Java Application
3. Open browser and hit http://localhost:8080/students/report

## How to Preview Data Manually without Spring Boot
1. Open sample.jrxml file then click Preview tab
2. Change DataAdapter in above Preview tab to "DB Dummy"
3. Fill the company then enter
4. You got an error right? :) 
5. Change testTime field type from java.time.LocalDate to java.sql.Date then save
6. Change testTime text field from $F{testTime}.format(java.time.format.DateTimeFormatter.ofPattern("dd-MM-yyyy")) to $F{testTime}.toLocalDate().format(java.time.format.DateTimeFormatter.ofPattern("dd-MM-yyyy")) then save
7. Fill the company again and then enter

## Note
For local date/time java 8+, if you done with preview manually then switch back to java.time.LocalDate/java.time.LocalDateTime again because java.sql.Date/java.sql.Timestamp only for preview purpose. 